#!/usr/bin/env python

# credit_cards2 (необязательная)
#
# Дополнительно нужно проверять и генерировать начальные цифры.
#
# Реализуйте программу для генерирования и проверки валидности
# номеров кредитных карт трёх видов:
#   - mastercard 16 цифр, начинается с 51-55 либо с 2221-2720
#   - americanexpress 15 цифр, начинается с 34 либо с 37
#   - visa 13, 16 или 19 цифр, начинается с 4

import pytest

class CreditCard():
    def __init__(self, number):
        # Сохраняет number в поле класса
        pass

    def is_valid(self):
        # Проверяет начальные цифры, длину строки и код Луна
        return

    @classmethod
    def generate(cls):
        # Случайным образом (в случае визы) выбирается длина,
        # затем случайным же образом выбираются начальные цифры
        # потом остальные цифры, так чтобы «сошёлся» код Луна
        # Рекомендуется использовать choice и randint из random
        return

class MasterCard(CreditCard):
    pass

class AmericanExpress(CreditCard):
    pass

class Visa(CreditCard):
    pass

def test0():  assert MasterCard('').is_valid() == False
def test1():  assert MasterCard('23978').is_valid() == False
def test2():  assert MasterCard('1234 5678 9012 3456').is_valid() == False
def test3():  assert MasterCard('5578 2350 9610 0287').is_valid() == True
def test31():  assert Visa('5578 2350 9610 0287').is_valid() == False
def test4():  assert MasterCard('5578 2350 9610 2087').is_valid() == False
def test5():  assert AmericanExpress('3473 170111 86210').is_valid() == True
def test6():  assert AmericanExpress('3473 170011 86210').is_valid() == False
def test7():  assert AmericanExpress('5578 2350 9610 0287').is_valid() == False
def test8():  assert Visa('4929 5958 3592 5180').is_valid() == True

def test9():
    for i in range(1000):
        n = Visa.generate()
        assert Visa(n).is_valid() == True
        assert MasterCard(n).is_valid() == False

if __name__ == '__main__':
    # При таком способе вызова каждый assert вместо просто да/нет будет
    # выдавать более детальную информацию, если что-то пошло не так.
    pytest.main([__file__])
#    pytest.main(['__file__ + '::test3'])    # запускает только третий тест
#    pytest.main(['-s', __file__ + '::test3'])   # то же + возможность отладки ipdb
