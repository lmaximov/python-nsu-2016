import pytest
from itertools import *
import operator
import collections

# Все функции должны возвращать итератор.

# В реализации допускается использование как генераторов,
# так и helper'ов из itertools, builtins и пр.

def tail(iterable, n):
    "Return an iterator over the last n items"
    pass

def pairs(iterable):
    """ Сгруппируйте элементы итерабельного объекта попарно:
    (1, 2, 3, 4, 5, 6) -> [(1, 2), (3, 4), (5, 6)]
    Если длина нечетная, последний элемент можно игнорировать
    На входе - итерабельный объект, на выходе - итератор

    """
    pass

def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    pass

def iter_except(func, exception, first=None):
    """ Call a function repeatedly until an exception is raised.

    Converts a call-until-exception interface to an iterator interface.
    Like builtins.iter(func, sentinel) but uses an exception instead
    of a sentinel to end the loop.

    """
    pass


def test1():
    assert ''.join(tail('ABCDEFG', 3)) == 'EFG'

def test2():
    def to_be():
        for i in 'tobeornottobe':
            yield i
    assert list(pairs([])) == []
    assert list(pairs([1, 2])) == [(1, 2)]
    assert list(pairs([1, 2, 3])) == [(1, 2)]
    assert list(pairs([1, 2, 3, 4, 5, 6])) == [(1, 2), (3, 4), (5, 6)]
    assert list(pairs('abcdefg')) == [('a', 'b'), ('c', 'd'), ('e', 'f')]
    assert list(pairs(to_be())) == [('t', 'o'), ('b', 'e'), ('o', 'r'),
                                    ('n', 'o'), ('t', 't'), ('o', 'b')]

def test3():
    assert ''.join(unique_everseen('AAAABBBCCDAABBB')) == 'ABCD'
    assert ''.join(unique_everseen('ABBcCAD', str.lower)) == 'ABcD'

def test4():
    d = collections.deque(('some', 'like', 'it', 'hot'))
    assert list(iter_except(d.pop, IndexError)) == ['hot', 'it', 'like', 'some']


if __name__ == '__main__':
    # При таком способе вызова каждый assert вместо просто да/нет будет
    # выдавать более детальную информацию, если что-то пошло не так.
    pytest.main([__file__])
#    pytest.main(['__file__ + '::test3'])    # запускает только третий тест
#    pytest.main(['-s', __file__ + '::test3'])   # то же + возможность отладки ipdb
