import re

# Найдите в тексте слова содержащие букву ё и выведите
# места, где они были найдены (индексы первых букв слов)
def yo(text):
    return

# Дан пароль. Вам надо проверить его «сложность», для 
# этого нужно подсчитать количество строчных, заглавных букв, цифр,
# знаков препинания и не-ascii символов. Вернуть tuple из 5 чисел.
def strength(password):
    return

# Преобразуйте в строке ссылки в формате MarkDown в html,
# например, [НГУ](http://nsu.ru) -> <a href="http://nsu.ru">НГУ</a>
def markdown(string):
    return

# Реализуйте функцию sscanf_helper, поддерживающую
# следующие коды: %u %d %s %f без каких-либо модификаторов (а также %%),
# которая преобразовывала бы формат sscanf в формат регулярного 
# выражения, например '%u' -> '(\d+)'
def sscanf_helper(text):
    return

# Напишите функцию sscanf, которая, используя sscanf_helper
# выполняла бы разбор строки по шаблону и возвращала бы
# найденное в виде tuple.
# Если шаблон не найден – пустой tuple
def sscanf(s, fmt):
    return

def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print('%s got: %s expected: %s' % (prefix, repr(got), repr(expected)))

if __name__ == '__main__':
    print('yo')
    test(yo(''), [])
    test(yo('ёж'), [0])
    test(yo('У водоёма растёт ёлка, У ёлки колкая иголка.'), [2, 10, 17, 25])

    print()
    print('strength')
    test(strength('qwerty'), (6, 0, 0, 0, 0))
    test(strength('Qwerty123!'), (5, 1, 3, 1, 0))
    test(strength('Пароль7'), (0, 0, 1, 0, 6))
    
    print()
    print('markdown')
    test(markdown('[Google](http://google.com)'), '<a href="http://google.com">Google</a>')
    test(markdown('[1][Google](http://google.com)'), '[1]<a href="http://google.com">Google</a>')
    test(markdown('[Google](http://google.com)(2)'), '<a href="http://google.com">Google</a>(2)')

    print()
    print('sscanf')
    test(sscanf('abc', '%d'), ())
    test(sscanf('10', '%u'), ('10',))
    test(sscanf('a=-5', '%s=%d'), ('a', '-5'))
    test(sscanf('a=-5%', '%s=%d%%'), ('a', '-5'))
    test(sscanf('a=16%, b=-5', '%s=%u%%, %s=%d'), ('a', '16', 'b', '-5'))
    test(sscanf('/usr/sbin/sendmail - 0 errors, 4 warnings', 
                '%s - %d errors, %d warnings'), 
               ('/usr/sbin/sendmail', '0', '4'))
    test(sscanf('-10, 1.2, .2; 1e-3, -2.3E4', '%d, %f, %f; %f, %f'), 
               ('-10', '1.2', '.2', '1e-3', '-2.3E4'))

