#!/usr/bin/env python3

def mult(a):
    return

assert mult([1, 2]) == [2, 1]
assert mult([1, 2, 3]) == [6, 3, 2]
assert mult([1, 2, 3, 4]) == [24, 12, 8, 6]
