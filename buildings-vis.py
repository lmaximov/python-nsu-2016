import time
from random import random

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from pylab import pause

fig1 = plt.figure()
ax1 = fig1.add_subplot(111, aspect='equal')
plt.ion()
for i in range(100):
    pause(0.01)
    x, y, dx, dy = random(), random(), random(), random()
    ax1.add_patch(
        patches.Rectangle((x-dx/2, y-dy/2), dx, dy, alpha=0.1)
    )
    plt.draw()

import ipdb; ipdb.set_trace()
