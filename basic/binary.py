#!/usr/bin/env python3

# В одной библиотеке функция GetTickCount ошибочно возвращает знаковое значение.
# Напишите битовый оператор (&, |, ^, ~), который превращал бы знаковое int32
# в беззнаковое uint32 без использования конструкции if/else.
#
# Задача усложняется тем, что в питоне используется длинная арифметика.
#
# Вообще, в питоне есть возможность сделать аналог reinterpret cast через void *,
# (см. test4() ниже) но попробуйте сделать это вручную.

# Для проверки решения выполните установку пакета pytest:
# pip install pytest
# затем запустите тесты:
# pytest binary.py

from random import randint
from struct import pack, unpack
import pytest

def cast(n):
    return

def test1():
    assert cast(10) == 10

def test2():
    assert cast(-1) == 2**32-1

def test3():
    a = -545265302
    b = 3749701994
    print('{:32b}'.format(a))   # ptest выводит это только
    print('{:32b}'.format(b))   # если тест не пройден
    #  -100000100000000001011010010110
    # 11011111011111111110100101101010
    assert cast(a) == b

def u2s(n):
    # uint32 -> int32
    return unpack('i', pack('I', n))[0]
    
def test4():
    for i in range(100):
        a = randint(0, 2**32-1)
        assert cast(u2s(a)) == a

if __name__ == '__main__':
    pytest.main([__file__])
