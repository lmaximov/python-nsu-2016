#!/usr/bin/env python3

# Реализуйте две функции, вычисляющую значение факториала:
# 1) при помощи цикла for
# 2) при помощи функции reduce
# Наличие двух различных реализаций помогает при тестировании

import pytest, math
from random import randint
from datetime import datetime as dt
from operator import mul
from functools import reduce
from decimal import Decimal as D

def fact1(n):
    # вычисление факториала числа при помощи цикла
    return

def fact2(n):
    # вычисление факториала числа при помощи функций
    # functools.reduce и operator.mul
    return

def fact3(n):
    # библиотечная функция
    return math.factorial(n)

def timer(f, n):
    # измерение времени выполнения
    t1 = dt.now()
    f(n)
    t2 = dt.now()
    print(t2-t1)

# граничные случаи
def check1(f):    assert f(1) == 1
def check2(f):    assert f(5) == 120
def check3(f):    assert len(str(f(10**4))) == 35660
def check4(f):    assert len(str(f(10**5))) == 456574

# граничные случаи первой реализации
def test11():   check1(fact1)
def test12():   check2(fact1)
def test13():   check3(fact1)

# граничные случаи второй реализации
def test21():   check1(fact2)
def test22():   check2(fact2)
def test23():   check3(fact2)

def test3():
    # тестирование сравнением различных реализаций
    for i in range(100):
        a = randint(1, 10**4)
        assert fact1(a) == fact2(a)

def test4():
    # тестирование сравнением с библиотечной функцией
    for i in range(100):
        a = randint(1, 10**4)
        assert fact1(a) == fact3(a)

def benchmark():
    # оценка скорости работы
    timer(fact1, 10**5)
    timer(fact2, 10**5)
    timer(fact3, 10**5)


if __name__ == '__main__':
    #fact2(5)                                               # запуск теста напрямую без pytest
    #pytest.main([__file__ + '::test21'])   # запуск одного теста
    #pytest.main(['-k', 'test1', __file__])                 # запуск тестов, содержащих в названии test1
    pytest.main([__file__])                                 # запуск всех тестов
    #benchmark()                                            # сравнение скорости работы

