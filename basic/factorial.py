#!/usr/bin/env python3

# Реализуйте функцию, вычисляющую значение факториала

import pytest, math
from random import randint

def fact(n):
    return

def test1():
    assert fact(1) == 1

def test2():
    assert fact(5) == 120

def test3():
    res = str(fact(10**4))
    assert res[:10] == '2846259680'
    assert len(res) == 35660

def test4():
    for i in range(100):
        a = randint(1, 10**4)
        assert fact(a) == math.factorial(a)

if __name__ == '__main__':
    pytest.main([__file__])
