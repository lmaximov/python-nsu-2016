#!/usr/bin/env python3
import pytest
from math import inf

# Напишите класс p_iter, который реализовывал бы
# обычной протокол итератора, но помимо этого
# позволял бы «подсмотреть» следующий элемент:
# >>> a = PeakableIterator('house')
# >>> a.peek
# 'h'
# >>> list(a)
# ['h', 'o', 'u', 's', 'e']

# При помощи этого итератора напишите функцию,
# которая принимала бы на вход два итерабельных объекта
# представляющие собой отсортированные последовательности
# и проводила бы «слияние» двух итераторов в один 
# с сохранением отсортированного порядка

class p_iter:
    pass

def merge(a, b):
    return

def test1():  assert list(merge([], [])) == []
def test2():  assert list(merge([1], [])) == [1]
def test3():  assert list(merge([], [1])) == [1]
def test4():  assert list(merge([1, 8, 9], [2, 5, 6])) == [1, 2, 5, 6, 8, 9]
def test5():  assert list(merge([1, 8], [2, 5, 6])) == [1, 2, 5, 6, 8]
def test6():  assert list(merge([1, 8, 9], [2, 5])) == [1, 2, 5, 8, 9]
def test7():  assert list(merge([-2, 0, inf], [-1, 8])) == [-2, -1, 0, 8, inf]
def test8():  assert list(merge('az', 'def')) == list('adefz')
def test9():  assert list(merge('aez', 'df')) == list('adefz')
def test10(): assert list(merge(( (), ((),(),()) ), 
                                ( ((),()),  ((),)*5 ) )) == \
                                [ (), ((),()), ((),(),()), ((),)*5 ]

if __name__ == '__main__':
    pytest.main([__file__])

