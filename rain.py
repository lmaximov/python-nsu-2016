def rain():
    return

assert rain([1, 3, 1]) == 0
assert rain([5, 1, 5]) == 4
assert rain([3, 1, 2, 4]) == 3
assert rain([3, 3, 1, 0, 2, 4, 2, 10]) == 8
