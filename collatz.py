from itertools import islice

# если элемент четный, то следующий a/2
# если элемент нечетный, то следующий 3*a+1

def collatz(n):
    return

def mylen(it):
    return

assert list(collatz(1)) == [1]
assert list(collatz(4)) == [4, 2, 1]
assert list(collatz(3)) == [3, 10, 5, 16, 8, 4, 2, 1]
assert mylen(collatz(75128138247)) == 1229

from matplotlib import pyplot as plt
a = list(islice(collatz(27), 150))
plt.plot(a)
plt.show()


