#!/usr/bin/env python3
from numpy import linspace, exp

def midpoint1(f, a, b, n):
    h = float(b-a)/n
    result = 0
    for i in range(n):
        result += f((a + h/2.0) + i*h)*h
    return result

def midpoint2(f, a, b, n):
    h = float(b-a)/n
    x = linspace(a + h/2, b - h/2, n)
    return h*sum(f(x))

f = lambda t: 3*t**2*exp(t**3)

for i in range(25):
    print('%3d %8d %.15f' % (i, 2**i, midpoint2(f, 0, 1, 2**i)))
