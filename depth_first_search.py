def dfs(it):
    return

assert list(dfs([])) == []
assert list(dfs([1])) == [1]
assert list(dfs([1, 2])) == [1, 2]
assert list(dfs([1, [2, 3, [], [4, 6, [7, 8]]], [10], 11])) == \
                [1, 2, 3, 4, 6, 7, 8, 10, 11]
