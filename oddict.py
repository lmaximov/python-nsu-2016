from collections import OrderedDict

class OrderedDefaultDict(OrderedDict):
    pass

def test1():
    a = OrderedDefaultDict(int)
    a['a'] += 1
    a['b'] += 2
    a['c'] += 3
    assert list(a.keys()) == ['a', 'b', 'c']
    del a['b']
    a['b'] += 4
    assert list(a.keys()) == ['a', 'c', 'b']

def test2():
    a = OrderedDefaultDict(list)
    a['q'].append(10)
    assert a['q'] == [10]
    assert repr(a) == "OrderedDefaultDict(<class 'list'>, [('q', [10])])"
    
def test3():
    import pickle
    from io import BytesIO
    a = OrderedDefaultDict()
    a['q'] = 10
    # ��� �������� ������ � ������ �� �����, ������ ��� ������ �����
    # with open('data.pkl', 'wb') as f:
    #     pickle.dump(a, f)
    #     pickle.dump(b, f)
    # ����� ��� ������
    # with open('data.pkl', 'rb') as f:
    #     a = pickle.load(f)
    #     b = pickle.dump(f)
    with BytesIO() as f:
        pickle.dump(a, f)
        f.seek(0)
        b = pickle.load(f)
    assert b['q'] == 10

def test4():
    from copy import copy, deepcopy
    a = OrderedDefaultDict()
    z = [100]
    a['q'] = 10
    a['w'] = z
    b1 = copy(a)
    b2 = deepcopy(a)
    a['q'] = 20
    a['w'][0] = 200
    assert b1['q'] == 10
    assert b2['q'] == 10
    assert b1['w'][0] == 200
    assert b2['w'][0] == 100

if __name__ == '__main__':
    import pytest
    pytest.main(['-s', __file__])
