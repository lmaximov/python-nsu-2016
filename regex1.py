#!/usr/bin/env python3

import re

# Найдите в тексте англ слова, заканчивающиеся на ly
# и выделите их при помощи тегов <i> и </i>,
# например "drive carefully" -> "drive <i>carefully</i>"
def mark_adverbs(text):
    # +++your code here+++
    return

# Найдите англ. и русские слова, в которые есть три (возможно, разных) 
# гласных подряд и возвратите их в нижнем регистре в виде списка
# в порядке появления в тексте
# Гласными здесь считаются буквы aeuioуеоаыяиюэ
def triple_vowels(text):
    # +++your code here+++
    return

# Найдите слова, в которые есть две одинаковые гласных подряд.
# Возвратите отсортированный список без повторов
def double_vowels(text):
    # +++your code here+++
    return

# Дана строка.
# Найдите первое вхождение слов 'не' и 'плох'.
# Если 'плох' идет после 'не' - замените всю подстроку
# 'не'...'плох' на 'хорош'.
# Верните получившуюся строку
# Например, not_bad('Этот ужин не так уж и плох!') вернет:
# 'Этот ужин хорош!'
def not_bad(s):
    # +++ ваш код +++
    return


def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print('%s got: %s expected: %s' % (prefix, repr(got), repr(expected)))


def main():
    print('mark_adverbs')
    test(mark_adverbs('He spoke quickly and angrily'), 
                      'He spoke <i>quickly</i> and <i>angrily</i>')
    test(mark_adverbs('Free lyrics web sites are "completely illegal"'),
                      'Free lyrics web sites are "<i>completely</i> illegal"')
    test(mark_adverbs('He meticulously called everybody from la to ly in the phonebook'), 
                      'He <i>meticulously</i> called everybody from la to ly in the phonebook')
    
    print()
    print('triple_vowels')
    test(triple_vowels('He saw a sihlouette of a beautiful queen.'), \
                      ['sihlouette', 'beautiful', 'queen'])
    test(triple_vowels('The ravioli was rather DELICIOUS!!'), ['delicious'])
    test(triple_vowels(u'Жираф - животное длинношеее'), ['длинношеее'])
    test(triple_vowels(u'Бррр, хооолодно!'), ['хооолодно'])

    print()
    print('double_vowels')
    test(double_vowels('В Кабардино-Балкарии валокордин из Болгарии'),
                      ['Балкарии', 'Болгарии'])
    test(double_vowels('They found many fishhooks as they stood by the brook.'), \
                      ['brook', 'fishhooks', 'stood'])
    test(double_vowels('Такая стратегия не целесообразна более'), \
                      ['более', 'целесообразна'])

    print()
    print('not_bad')
    test(not_bad('А негр был не плох'), 'А негр был хорош')
    test(not_bad('Да не просто не плох, а совсем не плох'), 'Да хорош, а совсем не плох') 
        # подсказки: аргумент count = количество;
        # если к * добавить ?, это будет означать >=0, но как можно меньше, т.н. nongreedy 

    test(not_bad('Да нет, он был совершенно не плох'), 'Да нет, он был совершенно хорош')
    test(not_bad('Неплохо, совсем не плохо'), 'Неплохо, совсем не плохо')
    test(not_bad('Не очень-то он и сплоховал'), 'Не очень-то он и сплоховал')
    test(not_bad('В стране уголь был, но он был плох'), 'В стране уголь был, но он был плох')

if __name__ == '__main__':
    main()
