import pytest

# Все нижеперечисленные функции должны возвращать итератор

# Выполните данное задание дважды: один раз через генераторы,
# другой раз через helper-функции из itertools и builtin.

def long_words(iterable, n=4):
    "Возвращает слова длиной не менее n символов"
    pass

def take(iterable, n):
    "Return first n items of the iterable as a list"
    pass

def tabulate(function, start=0):
    "Return function(0), function(1), ..."
    pass

def nth(iterable, n, default=None):
    "Returns the nth item or a default value, начиная с 0"
    pass

def dotproduct(vec1, vec2):
    pass

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    pass

def test0():
    assert list(long_words(
        'Карл клал лук на ларь Клара крала лук с ларя'.split())) == \
        ['Карл', 'клал', 'ларь', 'Клара', 'крала', 'ларя']

def test1():
    assert take([2, 4, 6, 8], 3) == [2, 4, 6]

def test2():
    assert take(tabulate(lambda x: x**2, 5), 3) == [25, 36, 49]

def test3():
    assert nth(tabulate(lambda x: 2*x, 7), 2) == 18

def test4():
    assert dotproduct([1, 2, 3], [2, 3, 4]) == 20

def test5():
    assert list(grouper('ABCDEFG', 3, 'x')) == \
            [('A','B','C'), ('D','E','F'), ('G','x','x')]

if __name__ == '__main__':
    # При таком способе вызова каждый assert вместо просто да/нет будет
    # выдавать более детальную информацию, если что-то пошло не так.
    pytest.main([__file__])
#    pytest.main(['__file__ + '::test3'])    # запускает только третий тест
#    pytest.main(['-s', __file__ + '::test3'])   # то же + возможность отладки ipdb
